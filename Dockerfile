FROM golang:1.22.1-alpine as builder

WORKDIR /app

COPY . /app

RUN go build -o main cmd/main.go

FROM alpine

WORKDIR /app

COPY --from=builder /app/main .

COPY .env .

COPY migrations /app/migrations

CMD ["/app/main"]

