// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.33.0
// 	protoc        v3.12.4
// source: product_selling_service.proto

package warehouse_service

import (
	empty "github.com/golang/protobuf/ptypes/empty"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CreateProductSellingRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Percent float32 `protobuf:"fixed32,2,opt,name=percent,proto3" json:"percent,omitempty"`
}

func (x *CreateProductSellingRequest) Reset() {
	*x = CreateProductSellingRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_product_selling_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateProductSellingRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateProductSellingRequest) ProtoMessage() {}

func (x *CreateProductSellingRequest) ProtoReflect() protoreflect.Message {
	mi := &file_product_selling_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateProductSellingRequest.ProtoReflect.Descriptor instead.
func (*CreateProductSellingRequest) Descriptor() ([]byte, []int) {
	return file_product_selling_service_proto_rawDescGZIP(), []int{0}
}

func (x *CreateProductSellingRequest) GetPercent() float32 {
	if x != nil {
		return x.Percent
	}
	return 0
}

type ProductSelling struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id        string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Percent   float32 `protobuf:"fixed32,2,opt,name=percent,proto3" json:"percent,omitempty"`
	CreatedAt string  `protobuf:"bytes,3,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt string  `protobuf:"bytes,4,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *ProductSelling) Reset() {
	*x = ProductSelling{}
	if protoimpl.UnsafeEnabled {
		mi := &file_product_selling_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProductSelling) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProductSelling) ProtoMessage() {}

func (x *ProductSelling) ProtoReflect() protoreflect.Message {
	mi := &file_product_selling_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProductSelling.ProtoReflect.Descriptor instead.
func (*ProductSelling) Descriptor() ([]byte, []int) {
	return file_product_selling_service_proto_rawDescGZIP(), []int{1}
}

func (x *ProductSelling) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *ProductSelling) GetPercent() float32 {
	if x != nil {
		return x.Percent
	}
	return 0
}

func (x *ProductSelling) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *ProductSelling) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type ProductSellingPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *ProductSellingPrimaryKey) Reset() {
	*x = ProductSellingPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_product_selling_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProductSellingPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProductSellingPrimaryKey) ProtoMessage() {}

func (x *ProductSellingPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_product_selling_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProductSellingPrimaryKey.ProtoReflect.Descriptor instead.
func (*ProductSellingPrimaryKey) Descriptor() ([]byte, []int) {
	return file_product_selling_service_proto_rawDescGZIP(), []int{2}
}

func (x *ProductSellingPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type GetProductSellingListRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Page   int32  `protobuf:"varint,1,opt,name=page,proto3" json:"page,omitempty"`
	Limit  int32  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetProductSellingListRequest) Reset() {
	*x = GetProductSellingListRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_product_selling_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetProductSellingListRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetProductSellingListRequest) ProtoMessage() {}

func (x *GetProductSellingListRequest) ProtoReflect() protoreflect.Message {
	mi := &file_product_selling_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetProductSellingListRequest.ProtoReflect.Descriptor instead.
func (*GetProductSellingListRequest) Descriptor() ([]byte, []int) {
	return file_product_selling_service_proto_rawDescGZIP(), []int{3}
}

func (x *GetProductSellingListRequest) GetPage() int32 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *GetProductSellingListRequest) GetLimit() int32 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetProductSellingListRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type ProductSellingResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ProductSellings []*ProductSelling `protobuf:"bytes,1,rep,name=product_sellings,json=productSellings,proto3" json:"product_sellings,omitempty"`
	Count           int32             `protobuf:"varint,2,opt,name=count,proto3" json:"count,omitempty"`
}

func (x *ProductSellingResponse) Reset() {
	*x = ProductSellingResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_product_selling_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProductSellingResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProductSellingResponse) ProtoMessage() {}

func (x *ProductSellingResponse) ProtoReflect() protoreflect.Message {
	mi := &file_product_selling_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProductSellingResponse.ProtoReflect.Descriptor instead.
func (*ProductSellingResponse) Descriptor() ([]byte, []int) {
	return file_product_selling_service_proto_rawDescGZIP(), []int{4}
}

func (x *ProductSellingResponse) GetProductSellings() []*ProductSelling {
	if x != nil {
		return x.ProductSellings
	}
	return nil
}

func (x *ProductSellingResponse) GetCount() int32 {
	if x != nil {
		return x.Count
	}
	return 0
}

var File_product_selling_service_proto protoreflect.FileDescriptor

var file_product_selling_service_proto_rawDesc = []byte{
	0x0a, 0x1d, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x73, 0x65, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x11, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22,
	0x37, 0x0a, 0x1b, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x18,
	0x0a, 0x07, 0x70, 0x65, 0x72, 0x63, 0x65, 0x6e, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x02, 0x52,
	0x07, 0x70, 0x65, 0x72, 0x63, 0x65, 0x6e, 0x74, 0x22, 0x78, 0x0a, 0x0e, 0x50, 0x72, 0x6f, 0x64,
	0x75, 0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x70, 0x65,
	0x72, 0x63, 0x65, 0x6e, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x02, 0x52, 0x07, 0x70, 0x65, 0x72,
	0x63, 0x65, 0x6e, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f,
	0x61, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61,
	0x74, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64,
	0x41, 0x74, 0x22, 0x2a, 0x0a, 0x18, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x65, 0x6c,
	0x6c, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x60,
	0x0a, 0x1c, 0x47, 0x65, 0x74, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c,
	0x69, 0x6e, 0x67, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12,
	0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x70, 0x61,
	0x67, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x65, 0x61, 0x72,
	0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68,
	0x22, 0x7c, 0x0a, 0x16, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69,
	0x6e, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x4c, 0x0a, 0x10, 0x70, 0x72,
	0x6f, 0x64, 0x75, 0x63, 0x74, 0x5f, 0x73, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x73, 0x18, 0x01,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x21, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52, 0x0f, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e,
	0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x32, 0xdb,
	0x03, 0x0a, 0x15, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x5d, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x12, 0x2e, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x50, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x1a, 0x21, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x65,
	0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x22, 0x00, 0x12, 0x57, 0x0a, 0x03, 0x47, 0x65, 0x74, 0x12, 0x2b,
	0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x1a, 0x21, 0x2e, 0x77, 0x61,
	0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x22, 0x00,
	0x12, 0x67, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x2f, 0x2e, 0x77, 0x61,
	0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x47, 0x65, 0x74, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x29, 0x2e, 0x77,
	0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x50, 0x0a, 0x06, 0x55, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x12, 0x21, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x53,
	0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x1a, 0x21, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75,
	0x73, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x22, 0x00, 0x12, 0x4f, 0x0a, 0x06, 0x44,
	0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x2b, 0x2e, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73,
	0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b,
	0x65, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x42, 0x1c, 0x5a, 0x1a,
	0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x77, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75,
	0x73, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x33,
}

var (
	file_product_selling_service_proto_rawDescOnce sync.Once
	file_product_selling_service_proto_rawDescData = file_product_selling_service_proto_rawDesc
)

func file_product_selling_service_proto_rawDescGZIP() []byte {
	file_product_selling_service_proto_rawDescOnce.Do(func() {
		file_product_selling_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_product_selling_service_proto_rawDescData)
	})
	return file_product_selling_service_proto_rawDescData
}

var file_product_selling_service_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_product_selling_service_proto_goTypes = []interface{}{
	(*CreateProductSellingRequest)(nil),  // 0: warehouse_service.CreateProductSellingRequest
	(*ProductSelling)(nil),               // 1: warehouse_service.ProductSelling
	(*ProductSellingPrimaryKey)(nil),     // 2: warehouse_service.ProductSellingPrimaryKey
	(*GetProductSellingListRequest)(nil), // 3: warehouse_service.GetProductSellingListRequest
	(*ProductSellingResponse)(nil),       // 4: warehouse_service.ProductSellingResponse
	(*empty.Empty)(nil),                  // 5: google.protobuf.Empty
}
var file_product_selling_service_proto_depIdxs = []int32{
	1, // 0: warehouse_service.ProductSellingResponse.product_sellings:type_name -> warehouse_service.ProductSelling
	0, // 1: warehouse_service.ProductSellingService.Create:input_type -> warehouse_service.CreateProductSellingRequest
	2, // 2: warehouse_service.ProductSellingService.Get:input_type -> warehouse_service.ProductSellingPrimaryKey
	3, // 3: warehouse_service.ProductSellingService.GetList:input_type -> warehouse_service.GetProductSellingListRequest
	1, // 4: warehouse_service.ProductSellingService.Update:input_type -> warehouse_service.ProductSelling
	2, // 5: warehouse_service.ProductSellingService.Delete:input_type -> warehouse_service.ProductSellingPrimaryKey
	1, // 6: warehouse_service.ProductSellingService.Create:output_type -> warehouse_service.ProductSelling
	1, // 7: warehouse_service.ProductSellingService.Get:output_type -> warehouse_service.ProductSelling
	4, // 8: warehouse_service.ProductSellingService.GetList:output_type -> warehouse_service.ProductSellingResponse
	1, // 9: warehouse_service.ProductSellingService.Update:output_type -> warehouse_service.ProductSelling
	5, // 10: warehouse_service.ProductSellingService.Delete:output_type -> google.protobuf.Empty
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_product_selling_service_proto_init() }
func file_product_selling_service_proto_init() {
	if File_product_selling_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_product_selling_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateProductSellingRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_product_selling_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProductSelling); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_product_selling_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProductSellingPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_product_selling_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetProductSellingListRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_product_selling_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProductSellingResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_product_selling_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_product_selling_service_proto_goTypes,
		DependencyIndexes: file_product_selling_service_proto_depIdxs,
		MessageInfos:      file_product_selling_service_proto_msgTypes,
	}.Build()
	File_product_selling_service_proto = out.File
	file_product_selling_service_proto_rawDesc = nil
	file_product_selling_service_proto_goTypes = nil
	file_product_selling_service_proto_depIdxs = nil
}
