package storage

import (
	"context"
	pbo "warehouse_service/genproto/organization_service_protos"
	pbw "warehouse_service/genproto/warehouse_service_protos"
)

type IStorage interface {
	Close()
	Income() IIncomeStorage
	IncomeProduct() IIncomeProductStorage
	ProductSelling() IProductSellingStorage
	WarehouseProduct() IWarehouseProductStorage
}

type IIncomeStorage interface {
	Create(context.Context, *pbw.CreateIncomeRequest) (*pbw.Income, error)
	Get(context.Context, *pbw.IncomePrimaryKey) (*pbw.Income, error)
	GetList(context.Context, *pbw.GetIncomeListRequest) (*pbw.IncomesResponse, error)
	Update(context.Context, *pbw.Income) (*pbw.Income, error)
	Delete(context.Context, *pbw.IncomePrimaryKey) (error)
	GenerateIncomeID(context.Context, string, *pbo.BranchNameResponse) (string, error)
	UpdateTotalSum(context.Context, float32, string) (error)
	FinishIncome(context.Context, *pbw.IncomePrimaryKey) (error)
}

type IIncomeProductStorage interface {
	Create(context.Context, *pbw.CreateIncomeProductRequest) (*pbw.IncomeProduct, error)
	Get(context.Context, *pbw.IncomeProductPrimaryKey) (*pbw.IncomeProduct, error)
	GetList(context.Context, *pbw.GetIncomeProductListRequest) (*pbw.IncomeProductsResponse, error)
	Update(context.Context, *pbw.IncomeProduct) (*pbw.IncomeProduct, error)
	Delete(context.Context, *pbw.IncomeProductPrimaryKey) (error)
}

type IProductSellingStorage interface {
	Create(context.Context, *pbw.CreateProductSellingRequest) (*pbw.ProductSelling, error)
	Get(context.Context, *pbw.ProductSellingPrimaryKey) (*pbw.ProductSelling, error)
	GetList(context.Context, *pbw.GetProductSellingListRequest) (*pbw.ProductSellingResponse, error)
	Update(context.Context, *pbw.ProductSelling) (*pbw.ProductSelling, error)
	Delete(context.Context, *pbw.ProductSellingPrimaryKey) (error)
}

type IWarehouseProductStorage interface {
	Create(context.Context, *pbw.CreateWarehouseProductRequest) (*pbw.WarehouseProduct, error)
	Get(context.Context, *pbw.WarehouseProductPrimaryKey) (*pbw.WarehouseProduct, error)
	GetList(context.Context, *pbw.GetWarehouseProductListRequest) (*pbw.WarehouseProductsResponse, error)
	Update(context.Context, *pbw.WarehouseProduct) (*pbw.WarehouseProduct, error)
	Delete(context.Context, *pbw.WarehouseProductPrimaryKey) (error)
	GetQuantityByBarcodeID(context.Context, string) (int, error)
 	UpdateData(context.Context, float32, int, float32, string) (error)
}
