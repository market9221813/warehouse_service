package postgres

import (
	"context"
	"fmt"
	"strings"
	pbo "warehouse_service/genproto/organization_service_protos"
	pbw "warehouse_service/genproto/warehouse_service_protos"
	"warehouse_service/storage"

	"github.com/jackc/pgx/v5/pgxpool"
)

type incomeRepo struct {
	DB *pgxpool.Pool
}

func NewIncomeRepo (db *pgxpool.Pool) storage.IIncomeStorage{
	return &incomeRepo{
		DB: db,
	}
}

func (i *incomeRepo) Create(ctx context.Context,createIncome *pbw.CreateIncomeRequest) (*pbw.Income, error) {
	income := pbw.Income{}

	query := `INSERT INTO incomes (id, branch_id, branch_selling_point_id, provider_id, total_sum, status)
			values ($1, $2, $3, $4, $5, $6) 
		returning id, branch_id, branch_selling_point_id, provider_id, total_sum, status::text, created_at::text `
 
		err := i.DB.QueryRow(ctx, query, 
		createIncome.GetId(), 
		createIncome.GetBranchId(),
		createIncome.GetBranchSellingPointId(),
		createIncome.GetProviderId(),
		createIncome.GetTotalSum(),
		createIncome.GetStatus(),
		).Scan(
		&income.Id,
		&income.BranchId,
		&income.BranchSellingPointId,
		&income.ProviderId,
		&income.TotalSum,
		&income.Status,
 		&income.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating Income!", err.Error())
		return &pbw.Income{},err
	}

	return &income, nil
}

func (i *incomeRepo) Get(ctx context.Context,pKey *pbw.IncomePrimaryKey) (*pbw.Income, error) {
	income := pbw.Income{}

	query := `SELECT id, branch_id, branch_selling_point_id, provider_id, total_sum, status::text, created_at::text, updated_at::text from incomes
				WHERE id = $1 AND deleted_at = 0 `
	err := i.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&income.Id,
		&income.BranchId,
		&income.BranchSellingPointId,
		&income.ProviderId,
		&income.TotalSum,
		&income.Status,
 		&income.CreatedAt,
		&income.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Income!", err.Error())
		return &pbw.Income{}, err
	}
	return &income, nil
}

func (i *incomeRepo) GetList(ctx context.Context, req *pbw.GetIncomeListRequest) (*pbw.IncomesResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		incomes = pbw.IncomesResponse{}
	)

	countQuery := `SELECT count(1) from incomes where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND id::text ilike '%%%s%%' `, search)
	}

	err := i.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of incomes!", err.Error())
		return &pbw.IncomesResponse{},err
	}

	query := `SELECT id, branch_id, branch_selling_point_id, provider_id, total_sum, status::text, 
	created_at::text, updated_at::text from incomes 
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND id::text ilike '%%%s%%' `, search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := i.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pbw.IncomesResponse{}, err
	}

	for rows.Next() {
		income := pbw.Income{}

		err := rows.Scan(
			&income.Id,
			&income.BranchId,
			&income.BranchSellingPointId,
			&income.ProviderId,
			&income.TotalSum,
			&income.Status,
 			&income.CreatedAt,
			&income.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning incomes!",err.Error())
			return &pbw.IncomesResponse{},err
		}

		incomes.Incomes = append(incomes.Incomes, &income)
	}

	incomes.Count = count

	return &incomes, nil
}

func (i *incomeRepo) Update(ctx context.Context, updIncome *pbw.Income) (*pbw.Income, error) {
	income := pbw.Income{}

	query := `UPDATE incomes SET branch_id = $1, branch_selling_point_id = $2, provider_id = $3, total_sum = $4, status = $5, 
		updated_at = NOW() 
			WHERE id = $6 AND deleted_at = 0 
		returning id, branch_id, branch_selling_point_id, provider_id, total_sum, status::text, updated_at::text `
	err := i.DB.QueryRow(ctx, query, 
		updIncome.GetBranchId(),
		updIncome.GetBranchSellingPointId(),
		updIncome.GetProviderId(),
		updIncome.GetTotalSum(),
		updIncome.GetStatus(),
		updIncome.GetId(),
		).Scan(

		&income.Id,
		&income.BranchId,
		&income.BranchSellingPointId,
		&income.ProviderId,
		&income.TotalSum,
		&income.Status,
 		&income.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating income!", err.Error())
		return &pbw.Income{}, err
	}

	return &income, nil

}

func (i *incomeRepo) Delete(ctx context.Context, pKey *pbw.IncomePrimaryKey) (error)  {
	query := `UPDATE incomes SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := i.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting Income!",err.Error())
		return err
	}
	return nil
}

func (i *incomeRepo) GenerateIncomeID(ctx context.Context, branchSellingPointID string, branchName *pbo.BranchNameResponse) (string, error){
	words := strings.Fields(branchName.GetName())

	var firstLetters string
	if len(words) > 1 {
		firstLetters = string(words[0][0]) + string(words[1][0])
	} else {
		firstLetters = string(words[0][0]) + string(words[0][1])
	}

	var count int
	query := `SELECT COUNT(*) FROM incomes WHERE branch_selling_point_id = $1 `

	err := i.DB.QueryRow(ctx,query, branchSellingPointID).Scan(&count)
	if err != nil {
		return "", err
	}
 	

 	newID := fmt.Sprintf("%s-%04d", strings.ToUpper(firstLetters), count+1)
	return newID, nil
}


func (i *incomeRepo) UpdateTotalSum(ctx context.Context, totalSum float32, id string) (error) {
	query := `UPDATE incomes SET total_sum = total_sum + $1 where id = $2 `

	_, err := i.DB.Exec(ctx, query, totalSum, id)
	if err != nil{
		fmt.Println("error while updating income total_sum!", err.Error())
		return err
	}

	return nil
}

func (i *incomeRepo) FinishIncome(ctx context.Context, pKey *pbw.IncomePrimaryKey) (error) {
	query := `UPDATE incomes SET status = 'completed' where id = $1`

	_, err := i.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while updating income status to completed!",err.Error())
		return err
	}
	return nil
}