package postgres

import (
	"context"
	"fmt"
	pb "warehouse_service/genproto/warehouse_service_protos"
	"warehouse_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type productSellingRepo struct {
	DB *pgxpool.Pool
}

func NewProductSellingRepo (db *pgxpool.Pool) storage.IProductSellingStorage{
	return &productSellingRepo{
		DB: db,
	}
}

func (p *productSellingRepo) Create(ctx context.Context,createProductSelling *pb.CreateProductSellingRequest) (*pb.ProductSelling, error) {
	productSelling := pb.ProductSelling{}

	query := `INSERT INTO product_sellings (id, percent)
			values ($1, $2) 
		returning id, percent, created_at::text `

	uid := uuid.New()

	err := p.DB.QueryRow(ctx, query, 
		uid, 
		createProductSelling.GetPercent(),
		).Scan(
		&productSelling.Id,
		&productSelling.Percent,
 		&productSelling.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating Product Selling!", err.Error())
		return &pb.ProductSelling{},err
	}

	return &productSelling, nil
}

func (p *productSellingRepo) Get(ctx context.Context,pKey *pb.ProductSellingPrimaryKey) (*pb.ProductSelling, error) {
	productSelling := pb.ProductSelling{}

	query := `SELECT id, percent, created_at::text, updated_at::text from product_sellings
				WHERE id = $1 AND deleted_at = 0 `
	err := p.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&productSelling.Id,
		&productSelling.Percent,
 		&productSelling.CreatedAt,
		&productSelling.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting ProductSelling!", err.Error())
		return &pb.ProductSelling{}, err
	}
	return &productSelling, nil
}

func (p *productSellingRepo) GetList(ctx context.Context, req *pb.GetProductSellingListRequest) (*pb.ProductSellingResponse, error) {
	var(
		count int32
		offset = (req.GetPage() - 1) * req.GetLimit()
		productSellings = pb.ProductSellingResponse{}
	)

	countQuery := `SELECT count(1) from product_sellings where deleted_at = 0 `

	err := p.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of product sellings!", err.Error())
		return &pb.ProductSellingResponse{},err
	}

	query := `SELECT id, percent, created_at::text, updated_at::text from product_sellings 
					WHERE deleted_at = 0 `

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := p.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.ProductSellingResponse{}, err
	}

	for rows.Next() {
		productSelling := pb.ProductSelling{}

		err := rows.Scan(
			&productSelling.Id,
			&productSelling.Percent,
 			&productSelling.CreatedAt,
			&productSelling.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning product sellings!",err.Error())
			return &pb.ProductSellingResponse{},err
		}

		productSellings.ProductSellings = append(productSellings.ProductSellings, &productSelling)
	}

	productSellings.Count = count

	return &productSellings, nil
}

func (p *productSellingRepo) Update(ctx context.Context, updIncome *pb.ProductSelling) (*pb.ProductSelling, error) {
	productSelling := pb.ProductSelling{}

	query := `UPDATE product_sellings SET percent = $1, updated_at = NOW() 
			WHERE id = $2 AND deleted_at = 0 
		returning id, percent, updated_at::text `
	err := p.DB.QueryRow(ctx, query, 
		updIncome.GetPercent(),
		updIncome.GetId(),
		).Scan(
		&productSelling.Id,
		&productSelling.Percent,
 		&productSelling.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating product sellings!", err.Error())
		return &pb.ProductSelling{}, err
	}

	return &productSelling, nil

}

func (p *productSellingRepo) Delete(ctx context.Context, pKey *pb.ProductSellingPrimaryKey) (error)  {
	query := `UPDATE product_sellings SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := p.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting ProductSelling!",err.Error())
		return err
	}
	return nil
}