package postgres

import (
	"context"
	"fmt"
	pb "warehouse_service/genproto/warehouse_service_protos"
	"warehouse_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type incomeProductRepo struct {
	DB *pgxpool.Pool
}

func NewIncomeProductRepo (db *pgxpool.Pool) storage.IIncomeProductStorage{
	return &incomeProductRepo{
		DB: db,
	}
}

func (i *incomeProductRepo) Create(ctx context.Context,createIncomeProduct *pb.CreateIncomeProductRequest) (*pb.IncomeProduct, error) {
	incomeProduct := pb.IncomeProduct{}

	query := `INSERT INTO income_products (id, category_id, product_name, barcode, quantity, price, income_id)
			values ($1, $2, $3, $4, $5, $6, $7) 
		returning id, category_id, product_name, barcode, quantity, price, income_id, created_at::text `

	uid := uuid.New()

	err := i.DB.QueryRow(ctx, query, 
		uid, 
		createIncomeProduct.GetCategoryId(),
		createIncomeProduct.GetProductName(),
		createIncomeProduct.GetBarcode(),
		createIncomeProduct.GetQuantity(),
		createIncomeProduct.GetPrice(),
		createIncomeProduct.GetIncomeId(),
 		).Scan(
		&incomeProduct.Id,
		&incomeProduct.CategoryId,
		&incomeProduct.ProductName,
		&incomeProduct.Barcode,
		&incomeProduct.Quantity,
		&incomeProduct.Price,
		&incomeProduct.IncomeId,
  		&incomeProduct.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating Income Product!", err.Error())
		return &pb.IncomeProduct{},err
	}

	return &incomeProduct, nil
}

func (i *incomeProductRepo) Get(ctx context.Context,pKey *pb.IncomeProductPrimaryKey) (*pb.IncomeProduct, error) {
	incomeProduct := pb.IncomeProduct{}

	query := `SELECT id, category_id, product_name, barcode, quantity, price, income_id, created_at::text, updated_at::text from income_products
				WHERE id = $1 AND deleted_at = 0 `
	err := i.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&incomeProduct.Id,
		&incomeProduct.CategoryId,
		&incomeProduct.ProductName,
		&incomeProduct.Barcode,
		&incomeProduct.Quantity,
 		&incomeProduct.Price,
		&incomeProduct.IncomeId,
 		&incomeProduct.CreatedAt,
		&incomeProduct.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting IncomeProduct!", err.Error())
		return &pb.IncomeProduct{}, err
	}
	return &incomeProduct, nil
}

func (i *incomeProductRepo) GetList(ctx context.Context, req *pb.GetIncomeProductListRequest) (*pb.IncomeProductsResponse, error) {
	var(
		count int32
		search = req.Search
		offset = (req.GetPage() - 1) * req.GetLimit()
		incomeProducts = pb.IncomeProductsResponse{}
	)

	countQuery := `SELECT count(1) from income_products where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND product_name ilike '%%%s%%' or barcode ilike '%%%s%%' `, search, search)
	}

	err := i.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of income!", err.Error())
		return &pb.IncomeProductsResponse{},err
	}

	query := `SELECT id, category_id, product_name, barcode, quantity, price, income_id, created_at::text, updated_at::text from income_products 
					WHERE deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(` AND product_name ilike '%%%s%%' or barcode ilike '%%%s%%' `, search, search)
	}

	query += `LIMIT $1 OFFSET $2`
	
	rows, err := i.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.IncomeProductsResponse{}, err
	}

	for rows.Next() {
		incomeProduct := pb.IncomeProduct{}

		err := rows.Scan(
			&incomeProduct.Id,
			&incomeProduct.CategoryId,
			&incomeProduct.ProductName,
			&incomeProduct.Barcode,
			&incomeProduct.Quantity,
			&incomeProduct.Price,
			&incomeProduct.IncomeId,
  			&incomeProduct.CreatedAt,
			&incomeProduct.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning income!",err.Error())
			return &pb.IncomeProductsResponse{},err
		}

		incomeProducts.IncomeProducts = append(incomeProducts.IncomeProducts, &incomeProduct)
	}

	incomeProducts.Count = count

	return &incomeProducts, nil
}

func (i *incomeProductRepo) Update(ctx context.Context, updIncomeProduct *pb.IncomeProduct) (*pb.IncomeProduct, error) {
	incomeProduct := pb.IncomeProduct{}

	query := `UPDATE income_products SET category_id = $1, product_name = $2, barcode = $3, quantity = $4, 
		price = $5, income_id = $6, 
		updated_at = NOW() 
			WHERE id = $7 AND deleted_at = 0 
		returning id, category_id, product_name, barcode, quantity, price, income_id, updated_at::text `
	err := i.DB.QueryRow(ctx, query, 
 		updIncomeProduct.GetCategoryId(),
 		updIncomeProduct.GetProductName(),
 		updIncomeProduct.GetBarcode(),
		updIncomeProduct.GetQuantity(),
		updIncomeProduct.GetPrice(),
		updIncomeProduct.GetIncomeId(),
 		updIncomeProduct.GetId(),
		).Scan(

		&incomeProduct.Id,
		&incomeProduct.CategoryId,
		&incomeProduct.ProductName,
		&incomeProduct.Barcode,
		&incomeProduct.Quantity,
		&incomeProduct.Price,
		&incomeProduct.IncomeId,
  		&incomeProduct.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating income!", err.Error())
		return &pb.IncomeProduct{}, err
	}

	return &incomeProduct, nil

}

func (i *incomeProductRepo) Delete(ctx context.Context, pKey *pb.IncomeProductPrimaryKey) (error)  {
	query := `UPDATE income_products SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := i.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting Income Product!",err.Error())
		return err
	}
	return nil
}