package postgres

import (
	"context"
	"fmt"
	"warehouse_service/config"
	"warehouse_service/storage"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Store struct {
	DB *pgxpool.Pool
	cfg config.Config
}

func New (ctx context.Context, cfg config.Config) (storage.IStorage, error){
	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)

	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil{
		fmt.Println("Error while parsing to pool config!",err.Error())
		return Store{}, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil{
		fmt.Println("Error while creating a new pool!",err.Error())
		return Store{},err
	}
 	
	return Store {
		DB: pool,
		cfg: cfg,
	}, nil
}

func (s Store) Close()  {
	s.DB.Close()
}

func (s Store) Income() storage.IIncomeStorage {
	return NewIncomeRepo(s.DB)
}

func (s Store) IncomeProduct() storage.IIncomeProductStorage {
	return NewIncomeProductRepo(s.DB)
}

func (s Store) ProductSelling() storage.IProductSellingStorage {
	return NewProductSellingRepo(s.DB)
}

func (s Store) WarehouseProduct() storage.IWarehouseProductStorage {
	return NewWarehouseProductRepo(s.DB)
}