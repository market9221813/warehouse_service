package postgres

import (
	"context"
	"fmt"
	pb "warehouse_service/genproto/warehouse_service_protos"
	"warehouse_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type warehouseProductRepo struct {
	DB *pgxpool.Pool
}

func NewWarehouseProductRepo (db *pgxpool.Pool) storage.IWarehouseProductStorage{
	return &warehouseProductRepo{
		DB: db,
	}
}

func (w *warehouseProductRepo) Create(ctx context.Context,createWarehouseProduct *pb.CreateWarehouseProductRequest) (*pb.WarehouseProduct, error) {
	warehouseProduct := pb.WarehouseProduct{}

	query := `INSERT INTO warehouse_products (id, branch_id, branch_selling_product_id, category_id, 
		barcode, income_product_price, quantity, total_sum)
			values ($1, $2, $3, $4, $5, $6, $7, $8) 
		returning id, branch_id, branch_selling_product_id, category_id, 
			barcode, income_product_price, quantity, total_sum, created_at::text `

	uid := uuid.New()

	err := w.DB.QueryRow(ctx, query, 
		uid, 
		createWarehouseProduct.GetBranchId(),
		createWarehouseProduct.GetBranchSellingProductId(),
		createWarehouseProduct.GetCategoryId(),
		createWarehouseProduct.GetBarcode(),
		createWarehouseProduct.GetIncomeProductPrice(),
		createWarehouseProduct.GetQuantity(),
		createWarehouseProduct.GetTotalSum(),
		).Scan(
		&warehouseProduct.Id,
		&warehouseProduct.BranchId,
		&warehouseProduct.BranchSellingProductId,
		&warehouseProduct.CategoryId,
		&warehouseProduct.Barcode,
		&warehouseProduct.IncomeProductPrice,
		&warehouseProduct.Quantity,
		&warehouseProduct.TotalSum,
 		&warehouseProduct.CreatedAt,
	)
	if err != nil{
		fmt.Println("error while creating warehouse product !", err.Error())
		return &pb.WarehouseProduct{},err
	}

	return &warehouseProduct, nil
}

func (w *warehouseProductRepo) Get(ctx context.Context,pKey *pb.WarehouseProductPrimaryKey) (*pb.WarehouseProduct, error) {
	warehouseProduct := pb.WarehouseProduct{}

	query := `SELECT id, branch_id, branch_selling_product_id, category_id, 
	barcode, income_product_price, quantity, total_sum, created_at::text, updated_at::text from warehouse_products
				WHERE id = $1 AND deleted_at = 0 `
	err := w.DB.QueryRow(ctx,query,pKey.Id).Scan(
		&warehouseProduct.Id,
		&warehouseProduct.BranchId,
		&warehouseProduct.BranchSellingProductId,
		&warehouseProduct.CategoryId,
		&warehouseProduct.Barcode,
		&warehouseProduct.IncomeProductPrice,
		&warehouseProduct.Quantity,
		&warehouseProduct.TotalSum,
 		&warehouseProduct.CreatedAt,
		&warehouseProduct.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Warehouse Product!", err.Error())
		return &pb.WarehouseProduct{}, err
	}
	return &warehouseProduct, nil
}

func (w *warehouseProductRepo) GetList(ctx context.Context, req *pb.GetWarehouseProductListRequest) (*pb.WarehouseProductsResponse, error) {
	var(
		count int32
		offset = (req.GetPage() - 1) * req.GetLimit()
		warehouseProducts = pb.WarehouseProductsResponse{}
	)

	countQuery := `SELECT count(1) from warehouse_products where deleted_at = 0 `

	err := w.DB.QueryRow(ctx,countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while selecting count of warehouse products!", err.Error())
		return &pb.WarehouseProductsResponse{},err
	}

	query := `SELECT id, branch_id, branch_selling_product_id, category_id, 
	barcode, income_product_price, quantity, total_sum, created_at::text, updated_at::text from warehouse_products 
					WHERE deleted_at = 0 `

	query += ` LIMIT $1 OFFSET $2`
	
	rows, err := w.DB.Query(ctx,query, req.Limit, offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.WarehouseProductsResponse{}, err
	}

	for rows.Next() {
		warehouseProduct := pb.WarehouseProduct{}

		err := rows.Scan(
			&warehouseProduct.Id,
			&warehouseProduct.BranchId,
			&warehouseProduct.BranchSellingProductId,
			&warehouseProduct.CategoryId,
			&warehouseProduct.Barcode,
			&warehouseProduct.IncomeProductPrice,
			&warehouseProduct.Quantity,
			&warehouseProduct.TotalSum,
		    &warehouseProduct.CreatedAt,
			&warehouseProduct.UpdatedAt,
		)

		if err != nil{
			fmt.Println("error while scanning  warehouse product !",err.Error())
			return &pb.WarehouseProductsResponse{},err
		}

		warehouseProducts.WarehouseProducts = append(warehouseProducts.WarehouseProducts, &warehouseProduct)
	}

	warehouseProducts.Count = count

	return &warehouseProducts, nil
}

func (w *warehouseProductRepo) Update(ctx context.Context, updIncome *pb.WarehouseProduct) (*pb.WarehouseProduct, error) {
	warehouseProduct := pb.WarehouseProduct{}

	query := `UPDATE warehouse_products SET branch_id = $1, branch_selling_product_id = $2, category_id = $3, 
	barcode = $4, income_product_price = $5, quantity = $6, total_sum = $7, updated_at = NOW() 
			WHERE id = $8 AND deleted_at = 0 
		returning id, branch_id, branch_selling_product_id, category_id, 
		barcode, income_product_price, quantity, total_sum, updated_at::text `
	err := w.DB.QueryRow(ctx, query, 
		updIncome.GetBranchId(),
		updIncome.GetBranchSellingProductId(),
		updIncome.GetCategoryId(),
		updIncome.GetBarcode(),
		updIncome.GetIncomeProductPrice(),
		updIncome.GetQuantity(),
		updIncome.GetTotalSum(),
		updIncome.GetId(),
		).Scan(
		&warehouseProduct.Id,
		&warehouseProduct.BranchId,
 		&warehouseProduct.BranchSellingProductId,
 		&warehouseProduct.CategoryId,
 		&warehouseProduct.Barcode,
 		&warehouseProduct.IncomeProductPrice,
 		&warehouseProduct.Quantity,
 		&warehouseProduct.TotalSum,
 		&warehouseProduct.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating warehouse product!", err.Error())
		return &pb.WarehouseProduct{}, err
	}

	return &warehouseProduct, nil

}

func (w *warehouseProductRepo) Delete(ctx context.Context, pKey *pb.WarehouseProductPrimaryKey) (error)  {
	query := `UPDATE warehouse_products SET deleted_at = 1
		WHERE id = $1 AND deleted_at = 0 `
	_, err := w.DB.Exec(ctx,query,pKey.Id)
	if err != nil{
		fmt.Println("error while deleting Warehouse Product!",err.Error())
		return err
	}
	return nil
}
	
func (w *warehouseProductRepo) UpdateData(ctx context.Context, price float32, quantity int, totalSum float32, barcode string) (error) {
	query := `UPDATE warehouse_products SET income_product_price = $1, quantity = quantity + $2,
			total_sum = $3 where barcode = $4`
	_, err := w.DB.Exec(ctx, query,
		price,
		quantity,
		totalSum,
		barcode,
	)
	if err != nil{
		fmt.Println("Error, This warehouse product doesn't exist!", err.Error())
		return err
	}

	return nil
}

func (w *warehouseProductRepo) GetQuantityByBarcodeID(ctx context.Context, barcode string) (int, error) {
	var quantity int
	query := `SELECT quantity from warehouse_products where barcode = $1 AND deleted_at = 0`

	err := w.DB.QueryRow(ctx, query, barcode).Scan(
		&quantity,
	)
	if err != nil{
		fmt.Println("error while getting quantity of warehouse products by barcode!", err.Error())
		return 0, err
	}
	return quantity, nil
}