CREATE TYPE income_status_enum AS ENUM (
    'in_process',
    'completed'
);

CREATE TABLE IF NOT EXISTS incomes (
    id varchar(10) primary key,
    branch_id uuid,
    branch_selling_point_id varchar(7),
    provider_id uuid,
    status income_status_enum,
    total_sum float,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS product_sellings (
    id uuid primary key,
    percent float,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS income_products (
    id uuid primary key,
    category_id uuid,
    product_name varchar(100),
    barcode varchar(10),
    quantity INT,
    price float,
    income_id varchar(10) references incomes(id),
    branch_id uuid,
    branch_selling_point_id varchar(7),
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS warehouse_products (
    id uuid primary key,
    branch_id uuid,
    branch_selling_product_id varchar(7),
    category_id uuid,
    barcode varchar(10),
    income_product_price float,
    quantity int,
    total_sum float,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    deleted_at INT DEFAULT 0
);

 