package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

type Config struct {
	PostgresHost     string
	PostgresPort     string
	PostgresUser     string
	PostgresPassword string
	PostgresDB       string

	ServiceName 	 string
	Environment      string

	ServiceGrpcHost string
	ServiceGrpcPort string

	OrganizationServiceGrpcHost string
	OrganizationServiceGrpcPort string

	CatalogServiceGrpcHost string
	CatalogServiceGrpcPort string

}

func Load() Config{
	cfg := Config{}

 	err := godotenv.Load()
	if err != nil{
		fmt.Println("Error while loading from godotenv!", err.Error())
		return Config{}
	}

	cfg.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST","localhost"))  
	cfg.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT","5432"))  
	cfg.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER","your_user"))  
	cfg.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD","your_password"))  
	cfg.PostgresDB = cast.ToString(getOrReturnDefault("POSTGRES_DB","your_database_name"))


	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME","service_name"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))

	cfg.ServiceGrpcHost = cast.ToString(getOrReturnDefault("SERVICE_GRPC_HOST","localhost"))  
	cfg.ServiceGrpcPort = cast.ToString(getOrReturnDefault("SERVICE_GRPC_PORT","8080"))  

	cfg.OrganizationServiceGrpcHost = cast.ToString(getOrReturnDefault("ORGANIZATION_SERVICE_GRPC_HOST","localhost"))  
	cfg.OrganizationServiceGrpcPort = cast.ToString(getOrReturnDefault("ORGANIZATION_SERVICE_GRPC_PORT","8080"))  

	cfg.CatalogServiceGrpcHost = cast.ToString(getOrReturnDefault("CATALOG_SERVICE_GRPC_HOST","localhost"))  
	cfg.CatalogServiceGrpcPort = cast.ToString(getOrReturnDefault("CATALOG_SERVICE_GRPC_PORT","8080"))  
	
	return cfg

}

func getOrReturnDefault (key string, defaultValue interface{})interface{}{
	value := os.Getenv(key)
	if value != ""{
		return value
	}
	return defaultValue
}