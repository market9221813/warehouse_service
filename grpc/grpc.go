package grpc

import (
	pb "warehouse_service/genproto/warehouse_service_protos"
	"warehouse_service/grpc/client"
	"warehouse_service/service"
	"warehouse_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer (strg storage.IStorage, services client.IServiceManager)*grpc.Server{
	grpcServer := grpc.NewServer()
	
	pb.RegisterIncomeServiceServer(grpcServer, service.NewIncomeService(strg, services))
	pb.RegisterIncomeProductServiceServer(grpcServer, service.NewIncomeProductService(strg, services))
	pb.RegisterProductSellingServiceServer(grpcServer, service.NewProductSellingService(strg, services))
	pb.RegisterWarehouseProductServiceServer(grpcServer, service.NewWarehouseProductService(strg, services))
	reflection.Register(grpcServer)

	return grpcServer
}