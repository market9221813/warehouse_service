package client

import (
	"fmt"
	"warehouse_service/config"
	catalog_service "warehouse_service/genproto/catalog_service_protos"
	organization_service "warehouse_service/genproto/organization_service_protos"
	warehouse_service "warehouse_service/genproto/warehouse_service_protos"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	IncomeService() warehouse_service.IncomeServiceClient
	IncomeProductService() warehouse_service.IncomeProductServiceClient
	ProductSellingService() warehouse_service.ProductSellingServiceClient
	WarehouseProductService() warehouse_service.WarehouseProductServiceClient

	// Organization service 
	BranchService() organization_service.BranchServiceClient
	BranchSellingPointService() organization_service.BranchSellingPointServiceClient

	// Catalog service

	ProductService() catalog_service.ProductServiceClient
	CategoryService() catalog_service.CategoryServiceClient
}

type grpcClients struct {
	incomeService warehouse_service.IncomeServiceClient
	incomeProductService warehouse_service.IncomeProductServiceClient
	productSellingService warehouse_service.ProductSellingServiceClient
	warehouseProductService warehouse_service.WarehouseProductServiceClient

	// Organiztion service
	branchService organization_service.BranchServiceClient
	branchSellingPointService organization_service.BranchSellingPointServiceClient
	// Catalog service 

	productService catalog_service.ProductServiceClient
	categoryService catalog_service.CategoryServiceClient
}

func NewGrpcClients (cfg config.Config)(IServiceManager, error){
	connWarehouseService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting warehouse_service!",err.Error())
		return nil, err
	}

	connOrganizationService, err := grpc.Dial(
		cfg.OrganizationServiceGrpcHost+cfg.OrganizationServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting organization_service!",err.Error())
		return nil, err
	}

	connCatalogService, err := grpc.Dial(
		cfg.CatalogServiceGrpcHost+cfg.CatalogServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting catalog_service!",err.Error())
		return nil, err
	}

	return &grpcClients{
		incomeService: warehouse_service.NewIncomeServiceClient(connWarehouseService),
		incomeProductService: warehouse_service.NewIncomeProductServiceClient(connWarehouseService),
		productSellingService: warehouse_service.NewProductSellingServiceClient(connWarehouseService),
		warehouseProductService: warehouse_service.NewWarehouseProductServiceClient(connWarehouseService),

		branchService: organization_service.NewBranchServiceClient(connOrganizationService),
		branchSellingPointService: organization_service.NewBranchSellingPointServiceClient(connOrganizationService),

		productService: catalog_service.NewProductServiceClient(connCatalogService),
		categoryService: catalog_service.NewCategoryServiceClient(connCatalogService),
	}, nil
}

func (g *grpcClients) IncomeService() warehouse_service.IncomeServiceClient{
	return g.incomeService
}

func (g *grpcClients) IncomeProductService() warehouse_service.IncomeProductServiceClient {
	return g.incomeProductService
}

func (g *grpcClients) ProductSellingService() warehouse_service.ProductSellingServiceClient {
	return g.productSellingService
}

func (g *grpcClients) WarehouseProductService() warehouse_service.WarehouseProductServiceClient {
	return g.warehouseProductService
}

// Organization service

func (g *grpcClients) BranchService() organization_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) BranchSellingPointService() organization_service.BranchSellingPointServiceClient {
	return g.branchSellingPointService
}

// Catalog service 

func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) CategoryService() catalog_service.CategoryServiceClient {
	return g.categoryService
}