package service

import (
	pb "warehouse_service/genproto/warehouse_service_protos"
	"warehouse_service/grpc/client"
	"warehouse_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type productSellingService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedProductSellingServiceServer
}

func NewProductSellingService (storage storage.IStorage, services client.IServiceManager) *productSellingService{
	return &productSellingService{
		storage: storage,
		services: services,
	}
}

func (p *productSellingService) Create(ctx context.Context, req *pb.CreateProductSellingRequest) (*pb.ProductSelling,error) {
	productSelling, err := p.storage.ProductSelling().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating Product Selling!", err.Error())
		return &pb.ProductSelling{},err
	}
	return productSelling, nil
}

func (p *productSellingService) Get(ctx context.Context, req *pb.ProductSellingPrimaryKey) (*pb.ProductSelling,error) {
	productSelling, err := p.storage.ProductSelling().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting Product Selling!", err.Error())
		return &pb.ProductSelling{},err
	}
	return productSelling, nil
}

func (p *productSellingService) GetList(ctx context.Context, req *pb.GetProductSellingListRequest) (*pb.ProductSellingResponse,error) {
	productSellings, err := p.storage.ProductSelling().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting product selling product list!",err.Error())
		return &pb.ProductSellingResponse{},err
	}
	return productSellings, nil
}

func (p *productSellingService) Update(ctx context.Context, req *pb.ProductSelling) (*pb.ProductSelling, error) {
	productSelling, err := p.storage.ProductSelling().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating Product Selling!",err.Error())
		return &pb.ProductSelling{}, err
	}
	return productSelling, nil
}

func (p *productSellingService) Delete(ctx context.Context, req *pb.ProductSellingPrimaryKey) (*emptypb.Empty,error) {
	err := p.storage.ProductSelling().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting Product Selling!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}