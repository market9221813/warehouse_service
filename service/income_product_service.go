package service

import (
	"context"
	"fmt"
	pbct "warehouse_service/genproto/catalog_service_protos"
	pbw "warehouse_service/genproto/warehouse_service_protos"
	"warehouse_service/grpc/client"
	"warehouse_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type incomeProductService struct {
	storage storage.IStorage
	services client.IServiceManager
	pbw.UnimplementedIncomeProductServiceServer
}

func NewIncomeProductService (storage storage.IStorage, services client.IServiceManager) *incomeProductService{
	return &incomeProductService{
		storage: storage,
		services: services,
	}
}

func (i *incomeProductService) Create(ctx context.Context, req *pbw.CreateIncomeProductRequest) (*pbw.IncomeProduct, error) {
	incomeProduct, err := i.storage.IncomeProduct().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating income product!", err.Error())
		return &pbw.IncomeProduct{},err
	}
	return incomeProduct,nil
}

func (i *incomeProductService) Get(ctx context.Context, req *pbw.IncomeProductPrimaryKey) (*pbw.IncomeProduct,error) {
	incomeProduct, err := i.storage.IncomeProduct().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting Income Product!", err.Error())
		return &pbw.IncomeProduct{},err
	}
	return incomeProduct, nil
}

func (i *incomeProductService) GetList(ctx context.Context, req *pbw.GetIncomeProductListRequest) (*pbw.IncomeProductsResponse,error) {
	incomeProducts, err := i.storage.IncomeProduct().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting income product list!",err.Error())
		return &pbw.IncomeProductsResponse{},err
	}
	return incomeProducts, nil
}

func (i *incomeProductService) Update(ctx context.Context, req *pbw.IncomeProduct) (*pbw.IncomeProduct, error) {
	incomeProduct, err := i.storage.IncomeProduct().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating Income Product!",err.Error())
		return &pbw.IncomeProduct{}, err
	}
	return incomeProduct, nil
}

func (i *incomeProductService) Delete(ctx context.Context, req *pbw.IncomeProductPrimaryKey) (*emptypb.Empty,error) {
	err := i.storage.IncomeProduct().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting Income Product!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}

func (i *incomeProductService) MakeIncome(ctx context.Context, req *pbw.CreateIncomeProductRequest) (*pbw.IncomeProduct,error) {
	_, err := i.services.CategoryService().Get(ctx, &pbct.CategoryPrimaryKey{Id: req.CategoryId})
   if err != nil{
	   fmt.Println("Error in service, this category id doesn't exist in categories service!",err.Error())
	   return &pbw.IncomeProduct{},err
   }

   productPriceResp, err := i.services.ProductService().GetPriceByBarcode(ctx, &pbct.ProductBarcodePrimaryKey{Barcode: req.Barcode})
   if err != nil{
	   fmt.Println("Error in service, This product id doesn't exist in products table!",err.Error())
	   return &pbw.IncomeProduct{},err
   }

   income, err := i.services.IncomeService().Get(ctx,&pbw.IncomePrimaryKey{Id: req.IncomeId})
   if err != nil{
	   fmt.Println("Service, This income id doesn't exist in incomes table!!!",err.Error())
	   return &pbw.IncomeProduct{},err
   }

   incomeProduct, err := i.storage.IncomeProduct().Create(ctx, req)
   if err != nil{
	   fmt.Println("Error in service, while creating Income Product!", err.Error())
	   return &pbw.IncomeProduct{},err
   }

   productName, err := i.services.ProductService().GetProductNameByBarcode(ctx, &pbct.ProductBarcodePrimaryKey{Barcode: req.Barcode})
   if err != nil{
		fmt.Println("Error in service, while getting product name!",err.Error())
		return &pbw.IncomeProduct{},err
   }

   incomeProduct.ProductName = productName.Name

   totalSum := req.Price * float32(req.Quantity)
 
   err = i.storage.Income().UpdateTotalSum(ctx, totalSum, req.IncomeId)
   if err != nil{
	   fmt.Println("Error in service, while updating income price!",err.Error())
	   return &pbw.IncomeProduct{},err
   }

   warehouseProductQuantity, err := i.storage.WarehouseProduct().GetQuantityByBarcodeID(ctx, req.Barcode)
   if err != nil{
	   fmt.Println("Error in service, while getting warehouse product quantity!", err.Error())
	   warehouseProductQuantity = 0
   }

   newWarehouseProductQuantity := float32(req.Quantity + int32(warehouseProductQuantity))

   i.storage.WarehouseProduct().UpdateData(ctx,req.Price, int(req.Quantity), req.Price * newWarehouseProductQuantity, req.Barcode)
	if err != nil{
	   fmt.Println("Service, This product doesn't exist in warehouse products, Creating a new!",err.Error())

	   _, err = i.services.WarehouseProductService().Create(ctx, &pbw.CreateWarehouseProductRequest{
		   BranchId: income.BranchId,
		   BranchSellingProductId: income.BranchSellingPointId,
		   CategoryId: incomeProduct.CategoryId,
 		   Barcode: req.Barcode,
 		   IncomeProductPrice: incomeProduct.Price,
		   Quantity: incomeProduct.Quantity,
		   TotalSum: incomeProduct.Price * float32(incomeProduct.Quantity),
	   })
	   if err != nil{
		   fmt.Println("Error in service, while creating new warehouse product depending on income product!",err.Error())
		   return &pbw.IncomeProduct{}, err
	   }
   }

   if productPriceResp.Price != req.Price{
	   _, err = i.services.ProductService().UpdateProductPrice(ctx, &pbct.UpdateProductPriceRequest{
		   Barcode: req.Barcode,
		   Price: req.Price,
	   })
	   if err != nil{
		   fmt.Println("Error in service, while updating product price!")
		   return &pbw.IncomeProduct{},err
	   }
   }

   return incomeProduct, nil
}
 
