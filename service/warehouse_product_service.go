package service

import (
	pb "warehouse_service/genproto/warehouse_service_protos"
	"warehouse_service/grpc/client"
	"warehouse_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type warehouseProductService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedWarehouseProductServiceServer
}

func NewWarehouseProductService (storage storage.IStorage, services client.IServiceManager) *warehouseProductService{
	return &warehouseProductService{
		storage: storage,
		services: services,
	}
}

func (w *warehouseProductService) Create(ctx context.Context, req *pb.CreateWarehouseProductRequest) (*pb.WarehouseProduct,error) {
	warehouseProduct, err := w.storage.WarehouseProduct().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating Warehouse Product!", err.Error())
		return &pb.WarehouseProduct{},err
	}
	return warehouseProduct, nil
}

func (w *warehouseProductService) Get(ctx context.Context, req *pb.WarehouseProductPrimaryKey) (*pb.WarehouseProduct,error) {
	warehouseProduct, err := w.storage.WarehouseProduct().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting Warehouse Product!", err.Error())
		return &pb.WarehouseProduct{},err
	}
	return warehouseProduct, nil
}

func (w *warehouseProductService) GetList(ctx context.Context, req *pb.GetWarehouseProductListRequest) (*pb.WarehouseProductsResponse,error) {
	warehouseProducts, err := w.storage.WarehouseProduct().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting warehouse product list!",err.Error())
		return &pb.WarehouseProductsResponse{},err
	}
	return warehouseProducts, nil
}

func (w *warehouseProductService) Update(ctx context.Context, req *pb.WarehouseProduct) (*pb.WarehouseProduct, error) {
	warehouseProduct, err := w.storage.WarehouseProduct().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating Warehouse Product!",err.Error())
		return &pb.WarehouseProduct{}, err
	}
	return warehouseProduct, nil
}

func (w *warehouseProductService) Delete(ctx context.Context, req *pb.WarehouseProductPrimaryKey) (*emptypb.Empty,error) {
	err := w.storage.WarehouseProduct().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting Warehouse Product!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}
