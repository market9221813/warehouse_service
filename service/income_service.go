package service

import (
	"context"
	"fmt"
 	pbo "warehouse_service/genproto/organization_service_protos"
	pbw "warehouse_service/genproto/warehouse_service_protos"

	"warehouse_service/grpc/client"
	"warehouse_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type incomeService struct {
	storage storage.IStorage
	services client.IServiceManager
	pbw.UnimplementedIncomeServiceServer
}

func NewIncomeService (storage storage.IStorage, services client.IServiceManager) *incomeService{
	return &incomeService{
		storage: storage,
		services: services,
	}
}

func (i *incomeService) Create(ctx context.Context, req *pbw.CreateIncomeRequest) (*pbw.Income,error) {
	branchName, err := i.services.BranchService().GetNameByID(ctx,&pbo.BranchPrimaryKey{Id: req.BranchId})
	if err != nil{
		fmt.Println("Error in service, while getting branch name by id!",err.Error())
		return &pbw.Income{},err
	}

	newID, err := i.storage.Income().GenerateIncomeID(ctx,req.BranchSellingPointId,branchName)
	if err != nil{
		fmt.Println("Error in service, while generating income id!", err.Error())
		return &pbw.Income{},err
	}

	req.Id = newID

	income, err := i.storage.Income().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while creating Income!", err.Error())
		return &pbw.Income{},err
	}
	return income, nil
}

func (i *incomeService) Get(ctx context.Context, req *pbw.IncomePrimaryKey) (*pbw.Income,error) {
	income, err := i.storage.Income().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while getting Income!", err.Error())
		return &pbw.Income{},err
	}
	return income, nil
}

func (i *incomeService) GetList(ctx context.Context, req *pbw.GetIncomeListRequest) (*pbw.IncomesResponse,error) {
	incomeProducts, err := i.storage.Income().GetList(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while getting income product list!",err.Error())
		return &pbw.IncomesResponse{},err
	}
	return incomeProducts, nil
}

func (i *incomeService) Update(ctx context.Context, req *pbw.Income) (*pbw.Income, error) {
	income, err := i.storage.Income().Update(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while updating Income!",err.Error())
		return &pbw.Income{}, err
	}
	return income, nil
}

func (i *incomeService) Delete(ctx context.Context, req *pbw.IncomePrimaryKey) (*emptypb.Empty,error) {
	err := i.storage.Income().Delete(ctx,req)
	if err != nil{
		fmt.Println("Error in service, while deleting Income!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}

func (i *incomeService) FinishIncome(ctx context.Context, req *pbw.IncomePrimaryKey) (*emptypb.Empty,error) {
	err := i.storage.Income().FinishIncome(ctx, req)
	if err != nil{
		fmt.Println("Error in service, while updating income status to comleted!",err.Error())
		return &emptypb.Empty{},err
	}
	return &emptypb.Empty{},nil
}
 